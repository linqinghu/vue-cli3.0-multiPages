import Vue from 'vue'
import App from './index.vue'

import 'src/assets/js/vue-common'
// import JsCallback from 'src/assets/js/js-callback';
import 'src/assets/js/ajax-util'

Vue.config.productionTip = false;

if(window.qqlgclientwebview || process.env.VUE_APP_DEBUG){
  let vm = new Vue({
    render: h => h(App)
  }).$mount('#app')
  // window.onload = () => {
  //   JsCallback.registerRefreshData(vm); //每个页面都注册
  // };
} else {
  window.document.body.innerHTML = '<h3>择天惠欢迎您！</h3>';
}


