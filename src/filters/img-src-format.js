/**
 * Created by fsj on 2017/2/27.
 */
import {CDN_BASE} from '../const/uri';

export const cdnImg = (imgSrc) => {
    if(!imgSrc || imgSrc === undefined || imgSrc.length === 0) {
        return CDN_BASE + '/static/img/logo.png';
    }

    if(imgSrc.startsWith('http://') || imgSrc.startsWith('https://') || imgSrc.startsWith('ftp://')) {
        return imgSrc;
    }

    if(!imgSrc.startsWith('/')) {
        imgSrc = '/' + imgSrc;
    }

    return CDN_BASE + imgSrc;
};
