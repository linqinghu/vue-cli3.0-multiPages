/**
 * Created by miidi on 2017/7/19.
 */
//赔率乘以10去除多个小数；
export const oddsRide=(arg)=>{
    if(!arg) return ;
    /*****/
    if(arg.toString().indexOf('.')==1){
        if(arg.toString().split(".")[1].length>3){
            arg=parseFloat(arg).toFixed(2)
        }
    }
    let arg1=10;
    let m = 0, s1 = arg1.toString();
    let s2 = arg.toString();
    try { m += s1.split(".")[1].length } catch (e) { }
    try { m += s2.split(".")[1].length } catch (e) { }
    return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m)
};
