
export const getCount=(number)=>{
    var n = 1;
    var num = ""+number;
    if(num.substring(num.indexOf(".")<0?0:num.indexOf(".")).length<=n){
        return number;
    }

    n = n ? parseInt(n) : 0;
    if (n <= 0) return Math.round(number);
    number = Math.round(number * Math.pow(10, n)) / Math.pow(10, n);
    return number;
}
