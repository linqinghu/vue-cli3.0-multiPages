
import {date} from  './date-format'

export const bijiaoshijian = (data) => {
  if(!data){ return }
  let nowTime=date(new Date);
  if(data.substr(0,10) == nowTime.substr(0,10)){
    return "今日"
  }else if(data<nowTime){
    return  data.substr(5,2)+"月"+data.substr(8,2)+'日';
  }
};
