import {normalTime} from './datetime-format';
import {cdnImg} from './img-src-format';
import {zero} from './zero-format';
import {date} from './date-format';
import {week} from './getWeek';
import {getCount} from './getcount'
import {oddsRide} from './oddsRide'
import {bijiaoshijian} from "./bijiaoshijian";

export default {
    normalTime, cdnImg, zero, date,week,getCount,oddsRide,bijiaoshijian
};
