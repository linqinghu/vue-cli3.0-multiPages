/**
 * Created by fsj on 2017/6/6.
 */

export default {
  /**
   * 获取url传过来的参数
   * @param name 参数名称
   * @param url URL地址
   * @param return
   */
  getQueryParam: function (name, url) {
    //URL GET 获取值
    var reg = new RegExp("(^|\\?|&)" + name + "=([^&]*)(\\s|&|$)", "i"),
        Url = url || location.href;
    if (reg.test(Url))
      return decodeURI(RegExp.$2.replace(/\+/g, " "));
    return "";

  },
  isApp: function () {
    if (window.qqlgclientwebview)
      return true;
    return false;
  },
  isIosApp: function () {
    if (window.qqlgioswebview)
      return true;
    return false;
  },
  isAndroidApp: function () {
    if (window.qqlgandroidwebview)
      return true;
    return false;
  },
  isWx: function () {
    if (navigator.userAgent.indexOf('MicroMessenger') > -1) {
      return true;
    }
    return false;

    // var ua = navigator.userAgent.toLowerCase();
    // 微信或者是uc
    // if (ua.match(/MicroMessenger/i) == "micromessenger" || ua.match(/qq\//i) == "qq/") {
    //
    //   return true;
    //
    // } else {
    //
    //   return false;
    //
    // }
  },
  /**
   * 区分 Adnroid || ios 终端或者浏览器；
   * @returns {boolean}
   */
  is_ios: function () {
    let  u = navigator.userAgent;
    if (!!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
      return true;
    } else {
      return false
    }
  },
  /*Android 终端*/
  is_Android:function(){
    let  u = navigator.userAgent;
    if(u.indexOf('Android') > -1 || u.indexOf('Adr') > -1){
      return true;
    }else{
      return false;
    }

  }
};

/**
 * 存储localStorage
 */
export const setStore = (name, content) => {
  if (!name) return;
  if (typeof content !== 'string') {
    content = JSON.stringify(content);
  }
  window.localStorage.setItem(name, content);
};

/**
 * 获取localStorage
 */
export const getStore = name => {
  if (!name) return;
  return window.localStorage.getItem(name);
};

