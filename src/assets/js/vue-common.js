/**
 * Created by fsj on 2017/6/6.
 */
// webpack.base.md-conf.js 中的 alias 做了配置，所以可以直接 src 开头的绝对路径
import 'src/assets/css/reset.css'
//import 'vux/src/styles/1px.less';
import 'src/assets/css/md-common.css';
import 'src/assets/css/common.less'

import Vue from 'vue';
import filters from 'src/filters';
import ImgSrcPlugin from 'src/plugins/img_src_plugin'
import Loadding from 'src/plugins/loading'
import VueLazyload from 'vue-lazyload'

import './rem';


// 关闭生产环境的调试信息
const isDebug_mode = process.env.NODE_ENV !== 'production'
Vue.config.debug = isDebug_mode
Vue.config.devtools = isDebug_mode
Vue.config.productionTip = isDebug_mode


//注册自定义过滤器
Object.keys(filters).forEach(key => Vue.filter(key, filters[key]));

Vue.use(ImgSrcPlugin);
Vue.use(Loadding);


Vue.use(VueLazyload, {
  error: `${process.env.VUE_APP_CDN_BASE}/static/img/imgerror.jpg`,
  loading: `${process.env.VUE_APP_CDN_BASE}/static/img/logo.jpg`,
  preLoad: 1,
  attempt: 1
});


//解决click点击300毫秒延时问题
import FastClick from 'fastclick';

FastClick.attach(document.body);
