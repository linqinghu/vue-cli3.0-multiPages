/**
 * Created by fsj on 2017/5/24.
 */

export const CDN_BASE = process.env.VUE_APP_CDN_BASE;

export const URI_BASE = process.env.VUE_APP_URI_BASE;

export const URI_LOGIN = URI_BASE + '/zthUser/userLogin';

//（6）接口名称：获取小编推荐指定模块的商品列表
export const URI_RECOMMENDDETAILS = URI_BASE + '/recommend/goods';

//我的收益
//export const URI_MYPROFIT = URI_BASE + '/parnter/security/income';

export const URI_MYPROFIT = URI_BASE + '/parnter/security/allIncome';

//订单详情
//export const URI_ORDERLIST = URI_BASE + '/parnter/security/orderList';
export const URI_ORDERLIST = URI_BASE + '/parnter/security/orderList2';

//提现记录
export const TIXIANRECOR = URI_BASE + '/parnter/security/tiXianRecord';

//我的粉丝

export const GET_FANS = URI_BASE + '/my/security/fans';

export const GET_FANSCOUNT = URI_BASE + '/my/security/fans2';

//（94）接口名称：获取我的粉丝数据
export const GET_MyFANS = URI_BASE + '/parnter/security/fans';

//发起提现申请

export const URI_TI_XIAN = URI_BASE + '/parnter/security/tiXian';

//红包列表
export const GET_HONGBAO = URI_BASE + '/hongbao/security/myHongbaoList';

//专属红包列表
export const GET_ZHUANSHU_HONGBAO = URI_BASE + '/hongbao/security/myZsHongbaoList';


//使用红包

export const USE_HONGBAO = URI_BASE + '/hongbao/security/useHongbao';

//专享红包使用

export const USE_ZX_HONGBAO = URI_BASE + '/hongbao/security/useZsHongbao';

//(24)接口名称：成为合伙人，查看当前用户状态

export const BECOMEPARTNERSTATUS = URI_BASE + '/parnter/parnterStatus';

//（41）接口名称：申请成为合伙人

export const APPLYPARTNER = URI_BASE + '/parnter/security/applyPartner';

// 获取用户信息；
export const GETUSERINFO = URI_BASE + '/my/security/userInfo';

//发送手机验证码

export const GET_Y_Z_CODE = URI_BASE + '/verifyCode/sendVerifyCode';

// (47)接口名称：创建淘口令

export const GET_TKL = URI_BASE + '/share/tkl';

//（46）接口名称：成为金牌合伙人

export const BECOMEHIGHTPARTNER = URI_BASE + '/parnter/security/becomeHightPartner';
//(20)接口名称:我的页面查询提现金额 1、2粉丝收益，累计提现
export const GETTIXIANMONEY = URI_BASE + '/my/security/profit';

//接口名称：修改用户支付宝信息

export const UPDATEZFBINFO = URI_BASE + '/parnter/security/updateZfbInfo';

//系统消息
export const MESSAGE = URI_BASE + '/message/security/list';

//message delete

export const MESSAGEDELETE = URI_BASE + '/message/security/delete';

//message details
export const MESSAGEDETAILS = URI_BASE + '/message/security/detail';

//用户流水

export const USERPROFITDETAILS = URI_BASE + '/parnter/security/userLog';

export const CHECKISLOGIN = URI_BASE + '/index/isLogin';

//（80）接口名称:查询活动商品
export const URI_GETGOODSACTIVITY = URI_BASE + '/good/goodsActivity';
//判断是否能领取红包
export const URI_IS_LINGQU_H_B = URI_BASE + '/activity/security/hongBaoStatus';

//（82）接口名称:使用免单红包
// 接口地址：/activity/security/useMdhb
export const URI_SHIYONG = URI_BASE + '/activity/security/useMdhb';

//（81） 接口名称：领取活动红包
// 接口地址：/activity/security/getHongBao

export const URI_GET_ACTIVITY_HONGBAO = URI_BASE + '/activity/security/getHongBao';

//升级码

export const USER_UPGRADECODE = URI_BASE + '/zthUser/security/upgradeCode';

//(87)获取答题红包
export const URI_GET_ANSWER_HONGBAO = URI_BASE + '/activity/security/getAnswerHongBao';

export const URI_GET_SHARE_CONTENT = URI_BASE + '/share/security/shareAnswerActivity';
//86）接口名称:使用答题红包
export const URI_USE_ANSWERHB = URI_BASE + '/activity/security/useAnswerhb';

//控制答题

export const URI_KONGZHI = URI_BASE + '/activity/isAnswered';
//(23)接口名称：新手输入邀请码获取红包（邀请人也会获取）

export const URI_GET_Y_Q_M_hh = URI_BASE + '/hongbao/security/sendHbWhenInvitated';

//orderFree 免单活动 接口名称：获取首页分类制定模块的商品列表 /good/queryByType

export const URI_GET_ORDERFREE = URI_BASE + '/good/queryByType';

//orderFree 页面中的判断接口 成功就跳详情；否则就提示

export const URI_GET_ORDERFREE_CHECK = URI_BASE + '/mdQualification/security/check';

export const URI_GET_SCROLLDATA = URI_BASE + '/good/goodMdRoll';

export const URI_GET_ORDERRANK = URI_BASE + '/parnter/orderRank';
