import {URI_BASE} from "./uri";


export const SHARE_URI_BASE = shareUriBase;

export const  GET_TKL = SHARE_URI_BASE + '/share/tkl';

export const  SAVECLICK = SHARE_URI_BASE + '/zthUserClick/saveClick';

//发送手机验证码

export const GET_Y_Z_CODE = SHARE_URI_BASE + '/verifyCode/sendVerifyCode';
//登录
export const URI_LOGIN = SHARE_URI_BASE + '/zthUser/userLogin';

//(88)接口名称：登录或注册  分享出来的需要填邀请码

export const URI_WEB_LOGIN = SHARE_URI_BASE + '/zthUser/userLoginInPage';

export const URI_WEB_LOGIN2 = SHARE_URI_BASE + '/zthUser/userLoginInPage2';

//(7) 接口名称：获取首页分类制定模块的商品列表
export const URI_GETLAYOUTTYPELIST = SHARE_URI_BASE + '/good/queryByType';

//wx分享
export const GETWXCONFIG = SHARE_URI_BASE + '/good/share';
