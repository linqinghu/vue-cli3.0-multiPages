/**
 * Created by fsj on 2017/2/27.
 */

export default {
    install(Vue) {

        Vue.directive('src', function (el, binding) {
            let imgSrc = binding.value;
            if (!imgSrc || imgSrc === 'undefined' || imgSrc.length === 0) {
                el.src = '/static/img/logo.jpg';
            } else if (imgSrc.indexOf('http://')==0 || imgSrc.indexOf('https://')==0 || imgSrc.indexOf('ftp://')==0) {
                el.src = imgSrc;
            } else {
                if (imgSrc.indexOf('/')!=0) {
                    imgSrc = '/' + imgSrc;
                }
                el.src = imgSrc;
            }
        });
    }
}
