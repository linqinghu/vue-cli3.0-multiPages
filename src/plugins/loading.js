
import Loadding from 'src/components/Loading'

export default {
  install: function(Vue) {
    Vue.component('loading', Loadding);
  }
}